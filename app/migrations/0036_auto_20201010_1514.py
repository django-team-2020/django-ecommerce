# Generated by Django 3.1.1 on 2020-10-10 08:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0035_auto_20201010_1512'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='name',
            field=models.CharField(max_length=220, verbose_name='Tên sự kiện'),
        ),
    ]
