from django import forms
from django.forms import ModelForm
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User # them model user

class CreateUserForm(UserCreationForm):
    password2 = forms.CharField(widget=forms.PasswordInput, label='nhập lại mật khẩu')
    class Meta:
        model = User

        fields = ['username','email', 'password1', 'password2']


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('name','slug', 'status')

class EventForm(ModelForm):
    class Meta:
        model = Event
        fields = '__all__'

class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'

class PostForm(ModelForm):
    class Meta:
        model = Product
        fields = ['content']

class ImagesProductForm(ModelForm):
    image = forms.ImageField(label='ảnh chi tiết')
    #main_image = forms.ImageField(label='ảnh chính')
    class Meta:
        model = ImagesProduct
        fields = ('image',)

