from django.shortcuts import render, redirect
from django.forms import modelformset_factory
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt,csrf_protect
from datetime import datetime
from .forms import *
from .models import *
from pprint import pprint
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from .decorators import *
from django.contrib.auth.models import Group, User

# PHAN LOGIN
@unauthenticated_user
def registerPage(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.is_staff = True
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='staff')
            user.groups.add(group) # khi dang ky, se tu dong them nguoi vua dang ky vao group staff

            messages.success(request, 'Tạo tài khoản thành công' + username)
            return redirect('login')
    context = {'form': form}
    return render(request, 'registration/register.html', context)

@unauthenticated_user
def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password) #authenticate can 3 tham so nhu tren
        # kiem tra user co ton tai hay khong
        if user is not None:
            #goi phuong thuc login da import
            login(request, user)
            return redirect('staff')
        else:
            messages.info(request, 'sai tài khoản hoặc mật khẩu!')
            return render(request, 'registration/login.html')

    context = {}
    return render(request,'registration/login.html',context)

def logoutUser(request):
    #su dung logout da import
    logout(request)
    return redirect('login')

#PHAN CATEGORY

@login_required(login_url='login')
#@allowed_users(allowed_roles=['admin']) # chi cho phep cac user nam trong group admin truy cap
def index(request):
    return render(request, 'index.html')

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def listCategory(request):
    listCate = Category.objects.all()
    return render(request, 'category/index.html', {'listCate': listCate})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def addCategory(request):
    form = CategoryForm()
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list-category')
        
    return render(request, 'category/add.html', {'form': form})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def updateCategory(request, pk):
    category = Category.objects.get(pk=pk)
    form = CategoryForm(instance=category)
    if request.method == 'POST':
        form = CategoryForm(request.POST ,instance=category)
        if form.is_valid():
            form.save()
            return redirect('list-category')
    return render(request, 'category/edit.html', {'form': form})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def deleteCategory(request, pk):
    category = Category.objects.get(pk=pk)
    category.delete()
    return redirect('list-category')

# PHẦN SẢN PHẨM

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def listProduct(request):
    products = Product.objects.all()
    return render(request, 'product/index.html', {'products': products})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def addProduct(request):
    ImagesProductFormSet = modelformset_factory(ImagesProduct, form=ImagesProductForm, extra=3)
    if request.method == 'POST':
        productForm = ProductForm(request.POST, request.FILES)
        formset = ImagesProductFormSet(request.POST, request.FILES, queryset=ImagesProduct.objects.none())
        
        if productForm.is_valid() and formset.is_valid():
            product_form = productForm.save(commit=False)
            product_form.save()

            for form in formset.cleaned_data:
                if form:
                    image = form['image']
                    photo = ImagesProduct(product=product_form, image=image)
                    photo.save()
                    
            messages.success(request, "Yeeew, check it out on the home page!")
            return redirect('list-product')
        else:
            print(productForm.errors, formset.errors)
    else:
        productForm = ProductForm()
        formset = ImagesProductFormSet(queryset=ImagesProduct.objects.none())
    return render(request, 'product/add.html', {'productForm': productForm, 'formset': formset})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def detailProduct(request, pk):
    p = Product.objects.get(pk=pk)
    imgP = ImagesProduct.objects.all()
    return render(request, 'product/detail.html', {'p': p, 'imgP': imgP})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def updateProduct(request, pk):
    ImagesProductFormSet = modelformset_factory(ImagesProduct, form=ImagesProductForm, extra=3)
    if request.method == 'POST':
        productForm = ProductForm(request.POST)
        formset = ImagesProductFormSet(request.POST, request.FILES, queryset=ImagesProduct.objects.none())
        
        if productForm.is_valid() and formset.is_valid():
            product_form = productForm.save(commit=False)
            product_form.save()

            for form in formset.cleaned_data:
                if form:
                    image = form['image']
                    photo = ImagesProduct(product=product_form, image=image)
                    photo.save()
                    
            messages.success(request, "Yeeew, check it out on the home page!")
            return redirect('list-product')
        else:
            print(productForm.errors, formset.errors)
    else:
        productForm = ProductForm()
        formset = ImagesProductFormSet(queryset=ImagesProduct.objects.none())
    return render(request, 'product/edit.html', {'productForm': productForm, 'formset': formset})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def deleteProduct(request, pk):
    product = Product.objects.get(pk=pk)
    product.delete()
    return redirect('list-product')

# PHẦN SỰ KIỆN
@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def listEvent(request):
    events = Event.objects.all()
    return render(request, 'events/index.html', {'events': events})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def addEvent(request):
    form = EventForm()
    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list-event')
    return render(request, 'events/add.html', {'form': form})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def editEvent(request, pk):
    event = Event.objects.get(pk=pk)
    form = EventForm(instance= event)
    if request.method == 'POST':
        form = EventForm(request.POST ,instance=event)
        if form.is_valid():
            form.save()
            return redirect('list-event')
    return render(request, 'event/edit.html', {'form': form})

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'staff'])
def deleteEvent(request, pk):
    event = Event.objects.get(pk=pk)
    event.delete()
    return redirect('list-event')


######## DEMO

def post(request):
    ImagesProductFormSet = modelformset_factory(ImagesProduct, form=ImagesProductForm, extra=3)
    if request.method == 'POST':
        productForm = ProductForm(request.POST)
        formset = ImagesProductFormSet(request.POST, request.FILES, queryset=ImagesProduct.objects.none())
        
        if productForm.is_valid() and formset.is_valid():
            product_form = productForm.save(commit=False)
            product_form.save()

            for form in formset.cleaned_data:
                if form:
                    image = form['image']
                    photo = ImagesProduct(product=product_form, image=image)
                    photo.save()
                    
            messages.success(request, "Yeeew, check it out on the home page!")
            return redirect('list-product')
        else:
            print(productForm.errors, formset.errors)
    else:
        productForm = ProductForm()
        formset = ImagesProductFormSet(queryset=ImagesProduct.objects.none())
    return render(request, 'demo.html', {'productForm': productForm, 'formset': formset})

def edit(request, id=None):
    product = get_object_or_404(Product, code=id)
    form = ProductForm(request.POST or None, instance=product)
    if form.is_valid():
        form.save()
        return redirect('')
    return render(request, 'demo.html', {'form': form})
