from django.urls import path
from .views_staff import *
from django.urls import include

urlpatterns = [
    #path('accounts/', include('django.contrib.auth.urls')),

    path('staff/register/', registerPage, name='register'),
    path('staff/login/', loginPage, name="login"),
	path('staff/logout/', logoutUser, name="logout"),

    path('staff', index, name='staff'),

    path('staff/category/', listCategory, name='list-category'),
    path('staff/category/add', addCategory, name='add-category'),
    path('staff/category/update/<pk>', updateCategory, name='update-category'),
    path('staff/category/delete/<pk>', deleteCategory, name='delete-category'),

    path('staff/product/', listProduct, name='list-product'),
    path('staff/product/add', addProduct, name='add-product'), 
    path('staff/product/update/<pk>', updateProduct, name='update-product'),
    path('staff/product/detail/<pk>', detailProduct, name='detail-product'),
    path('staff/product/delete/<pk>', deleteProduct, name='delete-product'),

    path('staff/event/', listEvent, name='list-event'),
    path('staff/event/add', addEvent, name='add-event'),
    path('staff/event/edit/<pk>', editEvent, name='edit-event'),
    path('staff/event/delete/<pk>', deleteEvent, name='add-event'),


    path('post', post, name='post'),
    path('edit/<pk>', edit, name='edit'),
    path('tinymce', include('tinymce.urls'))
]