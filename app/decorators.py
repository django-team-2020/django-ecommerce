from django.http import HttpResponse
from django.shortcuts import redirect

def unauthenticated_user(view_func):
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('staff')
        else:
            return view_func(request, *args, **kwargs)

    return wrapper_func

def allowed_users(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            group = None
            # kiem tra user co nam trong group ko?
            if request.user.groups.exists():
                group = request.user.groups.all()[0].name # lay ra phan tu dau tien va tra ve ten
            if group in allowed_roles:
                return view_func(request, *args, **kwargs)
            else:
                return HttpResponse('bạn không đủ quyền để truy cập chức năng này!')
        return wrapper_func
    return decorator


def admin_only(view_func):
    def wrapper_function(request, *args, **kwargs):
        group = None
        if request.user.groups.exists():
            group = request.user.groups.all()[0].name

        if group == 'customer':
            return redirect('staff')

        if group == 'admin' or group == 'staff':
            return view_func(request, *args, **kwargs)

    return wrapper_function