from django.urls import path
from .views_customer import *
from django.urls import include

urlpatterns = [
    path('home/login', login, name='login'),
    path('home/register', register, name='register'),
    path('home/logout', logout, name='logout'),

    path('home/', index, name='home'),
    path('home/product/<slug>', DetailProduct, name='detail-product')

]