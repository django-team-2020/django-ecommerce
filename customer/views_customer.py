from django.shortcuts import render, redirect
from app.models import *
from django.contrib.auth.models import Group, User
from django.contrib import messages
from django.contrib.auth import authenticate, login as customer_login, logout as customer_logout
from .forms import *
from .decorators import *

# Create your views here.

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password) #authenticate can 3 tham so nhu tren
        # kiem tra user co ton tai hay khong
        if user is not None:
            #goi phuong thuc login da import
            customer_login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'sai tài khoản hoặc mật khẩu!')
            return render(request, 'login.html')

    context = {}
    return render(request, 'login.html', context)

def register(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.is_staff = True
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='customer')
            user.groups.add(group)  # khi dang ky, se tu dong them nguoi vua dang ky vao group customer

            messages.success(request, 'Tạo tài khoản thành công với tên: ' + username)
            return redirect('login')
    context = {'form': form}
    return render(request, 'register.html', context)

def logout(request):
    customer_logout(request)
    return redirect('login')


def index(request):
    listProduct = Product.objects.all()
    context = {'listProduct': listProduct}
    return render(request, 'home/index.html', context)

def DetailProduct(request, slug):
    product = Product.objects.get(slug= slug)
    context = {'product': product}
    return render(request,'product/detail_product.html', context)