from django import forms
from django.forms import ModelForm
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User # them model user

class CreateUserForm(UserCreationForm):
    password2 = forms.CharField(widget=forms.PasswordInput, label='nhập lại mật khẩu')
    class Meta:
        model = User

        fields = ['username','email', 'password1', 'password2']